type binop = Sc | Lt | Lte | Gt | Gte | Neq | Eq | Add | Sub | Mult | Div | Mod
            | And | Or

type unop = Neg | Not
type nom = string


type params = (string * string) list

and variable = {vnom: string; mutable vvalue: expression; vtype_mj: string}

and methode  = {mnom: string; mbody: expression; mtype_mj: string; mparams:
    params; mstatic: bool}

and attribute = {anom: string; mutable avalue: expression; atype_mj: string;
    astatic: bool}

and attribute_or_method = Attribute of attribute | Methode of methode

and classe = {cnom: string; cbody: attribute_or_method list; cextends: string}

and classe_or_expr = Classe of classe| Expression of expression

and ast = classe_or_expr list

and expression =
    | Null 
    | True 
    | False
    | This
    | UnopExpr      of (unop * expression)
    | Nom           of nom
    | Int           of int
    | String        of string
    | BinopExpr     of (expression * binop * expression)
    | Assign        of (nom * expression)
    | Let           of (string * nom * expression * expression)
    | Condition     of (expression * expression * expression)
    | MethodCall    of (string * expression * args)
    | Instanciate   of string
    | Cast          of (string * expression)
    | Instanceof    of (expression * string)

and args = expression list

let binop_to_string = function
    Sc      -> ";"
    | Lt    -> "<"
    | Lte   -> "<="
    | Gt    -> ">"
    | Gte   -> ">="
    | Neq   -> "!="
    | Eq    -> "=="
    | Add   -> "+"
    | Sub   -> "-"
    | Mult  -> "*"
    | Div   -> "/"
    | Mod   -> "%"
    | And   -> "&&"
    | Or    -> "||"

let unop_to_string = function
    | Neg   -> "-"
    | Not   -> "!"

let rec params_to_string = function
    | []    -> ""
    | [(a,b)]   -> a ^ " " ^ b
    | (a,b)::c -> a ^ " " ^ b ^ ", " ^ (params_to_string c)

let rec attribute_or_method_list_to_string = function
    | []    -> ""
    | (Attribute a)::l -> (attribute_to_string a) ^ "\n" ^
                    (attribute_or_method_list_to_string l)
    | (Methode m)::l -> method_to_string m ^ "\n" ^
                    (attribute_or_method_list_to_string l)


and method_to_string = function
    m when m.mstatic ->
        "static " ^ m.mtype_mj ^ " " ^ m.mnom ^ " (" ^ (params_to_string
        m.mparams) ^ ") {\n" ^ expression_to_string
        m.mbody ^ "\n}"
    | m ->
        m.mtype_mj ^ " " ^ m.mnom ^ " (" ^ (params_to_string
        m.mparams) ^ ") {\n" ^ expression_to_string
        m.mbody ^ "\n}"

and attribute_to_string = function
    a when a.astatic ->
        "static " ^ a.atype_mj ^ " " ^ a.anom ^ "=" ^ (expression_to_string
        a.avalue) ^ ";"
    | a -> 
        a.atype_mj ^ " " ^ a.anom ^ "=" ^ (expression_to_string
        a.avalue) ^ ";"

and classe_to_string = function
    c when c.cextends = "Object" ->
        "class " ^ c.cnom ^ " {\n " ^ (attribute_or_method_list_to_string
        c.cbody) ^ "\n}\n\n"
    | c ->
        "class " ^ c.cnom ^ " extends " ^ c.cextends ^ " {\n "
        ^ (attribute_or_method_list_to_string c.cbody) ^ "\n}\n\n"

and ast_to_string = function
    | []                -> ""
    | (Classe c)::l     -> (classe_to_string c) ^ (ast_to_string l)
    | (Expression e)::l -> (expression_to_string e) ^ (ast_to_string l)

and expression_to_string = function
    | Null              -> "null"
    | True              -> "true"
    | False             -> "false"
    | This              -> "this"
    | UnopExpr(u, e)    -> (unop_to_string u) ^ "(" ^ (expression_to_string e) ^ ")"
    | Nom(n)            -> n
    | Int(i)            -> string_of_int i
    | String(s)         -> "\"" ^ s ^ "\""
    | BinopExpr(e1,b,e2)-> "(" ^ (expression_to_string e1) ^ (binop_to_string b)
    ^ (expression_to_string e2) ^ ")"
    | Assign(n,e1)      -> n ^ " = " ^ (expression_to_string e1)
    | Let(s,n,e1,e2)    -> s ^ " " ^ n ^ (expression_to_string e1) ^ " = " ^
    (expression_to_string e2)
    
    | Condition(e1,e2,e3)-> "if (" ^ (expression_to_string e1) ^ ") {\n" ^
    (expression_to_string e2) ^ "\n} else {\n" ^ (expression_to_string e3) ^ "\n}"
    
    | MethodCall(s,e,a) -> "(" ^ (expression_to_string e) ^ ")" ^ "." ^ s ^ "("
    ^ (args_to_string a) ^ ")"
    | Instanciate(s)    -> "new " ^ s
    | Cast(s,e)         -> "(" ^ s ^ ")" ^ " " ^ (expression_to_string e)
    | Instanceof(e,s)   -> (expression_to_string e) ^ " instanceof " ^ s

and args_to_string = function
    [] -> ""
    | [a] -> (expression_to_string a)
    | a::b -> (expression_to_string a) ^ ", " ^ (args_to_string b)

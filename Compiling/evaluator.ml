type value = Int of int
| String of string
| Boolean of bool
| null
| Object of int

type eval_type of string

type class_descriptor = int * (value list)

type object_descriptor = (eval_type * valeur) list

type evalattribute = string * value

let eval ast = ast.
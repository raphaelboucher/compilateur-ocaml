%{
    open Location
    open Exceptions
    open AST_types
%}

(* Opérateurs binaires *)
%token ADD MUL DIV SUB SC EQ LT LTE GT GTE NE ISEQUAL MOD AND OR

(* Opérateur unaire *)
%token EXMARK

(* Symboles *)
%token RACC LACC RPAR LPAR EOF COMMA DOT	

(* Mots-clés *)
%token CLASS EXTENDS STATIC NULL TRUE FALSE THIS IF ELSE NEW INSTANCEOF IN

%token <string> UIDENT
%token <string> LIDENT
%token <string> INT
%token <string> STRING



%start file
%type <AST_types.ast> file

%left letter
%left EQ
%left OR
%left AND
%left ISEQUAL NE
%left LT LTE GT GTE INSTANCEOF
%left ADD SUB
%left MUL DIV MOD
%right cast
%right unaire
%left DOT


%%
file:
    | c=class_or_expr_list EOF                  { c }
    | error                                     { print (symbol_loc $startpos $endpos); raise SyntaxError }
    
    

class_or_expr_list:
    |                                           { [] }
    | e=expr_list c=class_and_more              { Expression(e)::c }
    | c=class_and_more                          { c }


class_and_more:
    |                                           { [] }
    | CLASS c=UIDENT LACC b=method_or_attr_list RACC cl=class_or_expr_list 
        { Classe({cnom=c; cbody=b; cextends="Object" })::cl }

    | CLASS c=UIDENT EXTENDS e=UIDENT LACC b=method_or_attr_list RACC
    cl=class_or_expr_list
        { Classe({cnom=c; cbody=b; cextends=e })::cl }
    

method_or_attr_list:
    |                                           { [] }

(* Méthode non statique *)
    | t=UIDENT n=LIDENT LPAR p=params RPAR LACC b=expr_list RACC s=method_or_attr_list
        { Methode({mnom=n; mbody=b; mtype_mj=t; mparams=p; mstatic=false})::s }

(* Attribut non statique non initialisé *)
    | t=UIDENT n=LIDENT SC m=method_or_attr_list
        { Attribute({anom=n; avalue=Null; atype_mj=t; astatic=false})::m }

(* Attribut non statique initialisé *)
    | t=UIDENT n=LIDENT EQ e=expression SC m=method_or_attr_list  {
            Attribute({anom=n; avalue=e; atype_mj=t; astatic=false})::m }

(* Même chose on statique *)
    | st=STATIC t=UIDENT n=LIDENT LPAR p=params RPAR LACC b=expr_list RACC
    s=method_or_attr_list  {
            Methode({mnom=n; mbody=b; mtype_mj=t; mparams=p; mstatic=true})::s }

    | st=STATIC t=UIDENT n=LIDENT SC m=method_or_attr_list           {
            Attribute({anom=n; avalue=Null; atype_mj=t; astatic=true})::m }

    | st=STATIC t=UIDENT n=LIDENT EQ e=expression SC m=method_or_attr_list  {
            Attribute({anom=n; avalue=e; atype_mj=t; astatic=true})::m }


(* Une liste d'expressions: utile pour le corps des méthodes notamment *)
expr_list:
    |                                           { Null }
    | expression SC expr_list_tail              { BinopExpr($1, Sc, $3) }
    | e=expression SC                           { e }

expr_list_tail:
    | expression SC expr_list_tail              { BinopExpr($1, Sc, $3) }
    | e=expression SC                           { e }


expression:
    (* Entre parenthèses *)
    | LPAR e=expression RPAR                    { e }

    | i=INT                                     { Int(int_of_string i) }
    | s=STRING                                  { String(s) }
    (* Les mots-clés *)
    | NULL                                      { Null }
    | TRUE                                      { True }
    | FALSE                                     { False }
    | THIS                                      { This }

    (* Un identificateur seul *)
    | l=LIDENT                                  { Nom(l) }

    (* Les opérateurs *)
    | u=unop e=expression                       { UnopExpr(u, e) } %prec unaire
    | e1=expression b=binop e2=expression       { BinopExpr(e1, b, e2) }

    (* Assignation d'une variable *)
    | l=LIDENT EQ e=expression                  { Assign(l, e) }

    (* L'équivalent du let in de caml *)
    | u=UIDENT l=LIDENT EQ e1=expression IN e2=expression
            { Let(u, l, e1, e2) } %prec letter

    (* Structure conditionnelle *)
    | IF LPAR cond=expression RPAR
                LACC e1=expr_list RACC
                ELSE LACC e2=expr_list RACC    { Condition(cond, e1, e2) }
    | e=expression DOT l=LIDENT LPAR a=args RPAR  { MethodCall(l, e, a) }
    (* L'instanciation *)
    | NEW u=UIDENT                              { Instanciate(u) }

    (* Le cast *)
    | LPAR u=UIDENT RPAR e=expression           { Cast(u, e) } %prec cast

    | e=expression INSTANCEOF u=UIDENT          { Instanceof(e, u) }

(*
Plusieurs expressions à la suite: forcément entre parenthèses si pas dans un cas
habituel, c'est à dire directement dans le corps d'une méthode ou dans un if
*)
    | LPAR e1=expr_list RPAR                    { e1 }

(* Deux opérateurs unaires: - et ! *)
unop:
    | SUB                                       { Neg }
    | EXMARK                                    { Not }

(* Les opérateurs binaires *)
%inline binop:
    |LT                                         { Lt }
    |LTE                                        { Lte }
    |GT                                         { Gt }
    |GTE                                        { Gte }
    |NE                                         { Neq }
    |ISEQUAL                                    { Eq }
    |ADD                                        { Add }
    |SUB                                        { Sub }
    |MUL                                        { Mult }
    |DIV                                        { Div }
    |MOD                                        { Mod }
    |AND                                        { And }
    |OR                                         { Or }

(*
Les paramètres de fonctions: on distingue le premier des suivants, précédés
d'une virgule
*)
params:
	|                                           { [] }
    | t=UIDENT n=LIDENT s=not_first_params      { (t, n)::s }
not_first_params:
    |                                           { [] }
    | COMMA UIDENT LIDENT params                { ($2, $3)::$4 }

(*
Les arguments des fonctions: on distingue le premier des suivants, précédés
d'une virgule
*)
args:
    |                                           { [] }
    | e=expression a=not_first_args             { e::a }
not_first_args:
    |                                           { [] }
    | COMMA e=expression a=not_first_args       { e::a }

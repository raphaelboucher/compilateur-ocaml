{   
    open Lexing
    open Exceptions
    open Analyseur_syntaxique
    open Location
    open Buffer
}

let minletter = ['a'-'z']
let maxletter = ['A'-'Z']
let letter = minletter | maxletter
let digit = ['0'-'9']
let number = digit+('.'digit*)?
let lident = minletter(letter | digit | '_')*
let uident = maxletter(letter | digit | '_')*
let integer = digit+

let space = [' ' '\t']
let newline = ('\n' | '\r' | "\r\n" | "\n\r")


rule nexttoken = parse
    | newline                           { new_line lexbuf; nexttoken lexbuf }
    | space+                            {nexttoken lexbuf}
    | "class"                           { CLASS }
    | "extends"                         { EXTENDS }
    | "//"                              { parse_one_line_comment lexbuf}
    | "/*"                              { parse_multiline_comment lexbuf }
    | '"'                               { read_string (Buffer.create 16) lexbuf }
    | "null"                            { NULL }
    | "true"                            { TRUE }
    | "false"                           { FALSE }
    | "this"                            { THIS }
    | "if"                              { IF }
    | "else"                            { ELSE }
    | "new"                             { NEW }
    | "instanceof"                      { INSTANCEOF }
    | "in"                              { IN }
    | "static"                          { STATIC }
    | "{"                               { LACC }
    | "}"                               { RACC }
    | "("                               { LPAR }
    | ")"                               { RPAR }
    | ";"                               { SC }
    | "="                               { EQ }
    | "/"                               { DIV }
    | "*"                               { MUL }
    | "+"                               { ADD }
    | "-"                               { SUB }
    | ","                               { COMMA }
    | "!"                               { EXMARK }
    | "<"                               { LT }
    | "<="                              { LTE }
    | ">"                               { GT }
    | ">="                              { GTE }
    | "!="                              { NE }
    | "=="                              { ISEQUAL }
    | "%"                               { MOD }
    | "&&"                              { AND }
    | "."                               { DOT }
    | "||"                              { OR }
    | integer as num                    { INT(num) }
    | lident as l                       { LIDENT (l) }
    | uident as l                       { UIDENT (l) }
    | eof                               { EOF }
    | _         { let pos = Lexing.lexeme_start_p lexbuf in
                    raise (
                        FormatException ("Mauvais formatage a la ligne "^(string_of_int
                        pos.pos_lnum) ^ " au caractere " ^ (string_of_int
                        (pos.pos_cnum - pos.pos_bol + 1)))
                        )
                }
and parse_one_line_comment = parse
    | newline                           { new_line lexbuf; nexttoken lexbuf }
    | eof                               { EOF }
    | _                                 { parse_one_line_comment lexbuf }
and parse_multiline_comment = parse
    | "*/"                              { nexttoken lexbuf }
    | eof                               { EOF }
    | newline                           { new_line lexbuf;
    parse_multiline_comment lexbuf }
    | _                                 { parse_multiline_comment lexbuf }

and read_string buf = parse
    | '"'       { STRING (Buffer.contents buf) }
    | '\\' '"'  { Buffer.add_char buf '"'; read_string buf lexbuf }
    | '\\' '\\' { Buffer.add_char buf '\\'; read_string buf lexbuf }
    | '\\' 'n'  { Buffer.add_char buf '\n'; read_string buf lexbuf }
    | '\\' 'r'  { Buffer.add_char buf '\r'; read_string buf lexbuf }
    | '\\' 't'  { Buffer.add_char buf '\t'; read_string buf lexbuf }
    | _ as c { Buffer.add_char buf c; read_string buf lexbuf }
    | eof { raise (FormatException ("String is not terminated")) }